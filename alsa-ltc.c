/*
  sudo apt-get install libasound2-dev libltc-dev
   gcc -o alsa-ltc -lasound -llibltc alsa-ltc.c tinyosc/tinyosc.c && ./alsa-ltc hw:0
*/

#define LTC_QUEUE_LEN (8) // should be >> ( max(jack period size) * max-speedup / (duration of LTC-frame) )


#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in */
#include <unistd.h>     /* for close() */
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <string.h>
#include <ltc.h>
#include <signal.h>
#include "tinyosc/tinyosc.h"

#define ALSA_FORMAT SND_PCM_FORMAT_S16_LE
#define ALSA_CHANNELS 2
#define BUFFER_FRAMES 64

static unsigned int samplerate = 44100;
static LTCDecoder **decoder = NULL;
static int fps_num = 25;
static int fps_den = 1;

/* UDP stuff */
static int sock;                  /* Socket */
struct sockaddr_in broadcastAddr; /* Broadcast address */
char *broadcastIP;                /* IP broadcast address */
unsigned short broadcastPort;     /* Server port */

// handle Ctrl+C
static volatile bool keep_running = true;
static void sigintHandler(int x) {
  keep_running = false;
}

static void decoder_read(LTCDecoder *d, int channel) {
  LTCFrameExt frame;

  /* TinyOSC */
  // declare a buffer for writing the OSC packet into
  char osc_buffer[1024];
  char osc_string[256];
  char *osc_address;
  uint osc_len;

  if (channel == 0) {
    osc_address = "/clock/ltc";
  } else {
    osc_address = "/clock/ltc2";
  }

  while (ltc_decoder_read(d, &frame)) {
    SMPTETimecode stime;
    ltc_frame_to_time(&stime, &frame.ltc, /* use_date? LTC_USE_DATE : */ 0);

    sprintf(osc_string, "%02d:%02d:%02d:%02d",
      stime.hours,
      stime.mins,
      stime.secs,
      stime.frame
    );
    // printf("%d", channel);
    osc_len = tosc_writeMessage(osc_buffer, sizeof(osc_buffer),
      osc_address, // the address
      "s",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
      osc_string);
    if (sendto(sock, osc_buffer, osc_len, 0, (struct sockaddr *)
       &broadcastAddr, sizeof(broadcastAddr)) != osc_len) {
       fprintf(stderr, "Failed to send OSC packet!\n");
    }
  }
  // fflush(stdout);
}


// derived from alsa-utils/aplay.c
static char * alsa_enum_soundcards (void) {
    void **hints, **n;
    char *name, *descr, *io;
    const char *filter = "bcm2835";
    const char *null_name = "null";
    const char *io_filter = "Output";
    if (snd_device_name_hint(-1, "pcm", &hints) < 0)
        return NULL;
    n = hints;

    fprintf(stdout, "Detecting capture devices:\n");
    while (*n != NULL) {
        name = snd_device_name_get_hint(*n, "NAME");
        descr = snd_device_name_get_hint(*n, "DESC");
        io = snd_device_name_get_hint(*n, "IOID");

        if (strstr(descr, filter) == NULL && strstr(name, null_name) == NULL) {
          if (io == NULL || strstr(io, io_filter) == NULL) {
            fprintf(stdout, "Matched card:\n");
            fprintf(stdout, "Name: %s Desc: %s IO: %s\n", name, descr, io);
            snd_device_name_free_hint(hints);
            return name;
          }
        }

        if (name != NULL)
            free(name);
        if (descr != NULL)
            free(descr);
        if (io != NULL)
            free(io);
        n++;
    }
    snd_device_name_free_hint(hints);
    return NULL;
}

int main(int argc, char *argv[])
{
  int err;
  short *buffer;
  char *card;
  int buffer_frames = BUFFER_FRAMES;
  snd_pcm_t *capture_handle;
  snd_pcm_hw_params_t *hw_params;
  snd_pcm_format_t format = ALSA_FORMAT;
  int broadcastPermission;          /* Socket opt to set permission to broadcast */
  unsigned int channels;

  if (argc < 4) {
    fprintf(stderr,"Usage:  %s <alsa-device> <OSC destination ip> <OSC port>\n", argv[0]);
    fprintf(stderr,"Use - for device for automatic detection on raspberry pi\n");
    exit(1);
  }

  broadcastIP = argv[2];           /* First arg:  broadcast IP address */
  broadcastPort = atoi(argv[3]);    /* Second arg:  broadcast port */

  fprintf(stdout, argv[1]);

  if (argv[1][0] == '-') {
    card = alsa_enum_soundcards();
  } else {
    card = argv[1];
  }


  if ((err = snd_pcm_open(&capture_handle, card, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    fprintf(stderr, "cannot open audio device %s (%s)\n",
     card,
     snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "audio interface opened\n");

  if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
    fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
      snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params allocated\n");

  if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n",
      snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params initialized\n");

  if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_NONINTERLEAVED)) < 0) {
    fprintf(stderr, "cannot set access type (%s)\n",
      snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params access setted\n");

  if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, format)) < 0) {
    fprintf(stderr, "cannot set sample format (%s)\n",
      snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "hw_params format setted\n");

  if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &samplerate, 0)) < 0) {
    fprintf(stderr, "cannot set sample rate (%s)\n",
      snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "hw_params rate setted\n");


  err = snd_pcm_hw_params_get_channels_max(hw_params, &channels);
  if (err < 0)
    exit (1);
  fprintf(stdout, "Max channels: %u. (%d)\n", channels, err);

  if (channels > 2) {
    channels = 2;
  }
  fprintf(stdout,"Using %u channels\n", channels);

  if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, channels)) < 0) {
    fprintf(stderr, "cannot set channel count (%s)\n",
      snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "hw_params channels setted\n");

  if ((err = snd_pcm_hw_params (capture_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n",
      snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "hw_params setted\n");

  snd_pcm_hw_params_free(hw_params);

  fprintf(stdout, "hw_params freed\n");

  if ((err = snd_pcm_prepare(capture_handle)) < 0) {
    fprintf(stderr, "cannot prepare audio interface for use (%s)\n",
      snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "audio interface prepared\n");

  buffer = malloc(buffer_frames * sizeof(short) / 8 * channels);

  short * chan_buffer[2];
  for (int i = 0; i < channels; i++) {
    chan_buffer[i] = malloc(buffer_frames * snd_pcm_format_width(format) / 8);
  }


  fprintf(stdout, "buffer allocated\n");

  decoder = malloc(sizeof(LTCDecoder *) * channels);
  for (int i = 0; i < channels; i++) {
    decoder[i] = ltc_decoder_create(samplerate * fps_den / fps_num, LTC_QUEUE_LEN);
    fprintf(stdout, "LTC decoder initialized: sample rate: %d, fps: %d / %d\n", samplerate, fps_den, fps_num);
  }



  /* Create socket for sending/receiving datagrams */
  if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    fprintf(stderr, "socket() failed");
    exit(1);
  }
  /* Set socket to allow broadcast */
  broadcastPermission = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
    sizeof(broadcastPermission)) < 0) {
      fprintf(stderr, "setsockopt() failed");
      exit(1);
  }

  /* Construct local address structure */
  memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
  broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
  broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
  broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

  // register the SIGINT handler (Ctrl+C)
  signal(SIGINT, &sigintHandler);

  fprintf(stdout, "Running\n");

  while (keep_running) {
    if ((err = snd_pcm_readn (capture_handle, (void **)chan_buffer, buffer_frames)) != buffer_frames) {
      fprintf (stderr, "read from audio interface failed (%s)\n",
       snd_strerror (err));
      exit (1);
    }

    for (int i = 0; i < channels; i++) {
      ltc_decoder_write_s16(decoder[i], chan_buffer[i], buffer_frames, 0);
      decoder_read(decoder[i], i);
    }

  }

  free(buffer);

  fprintf(stdout, "buffer freed\n");

  snd_pcm_close (capture_handle);
  fprintf(stdout, "audio interface closed\n");

  exit (0);
}
